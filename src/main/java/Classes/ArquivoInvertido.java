/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kelvin
 */
public class ArquivoInvertido {
    public String palavra;
    public List<Integer> indices = new ArrayList<>();
    public ArquivoInvertido(String palavra,Integer indice){
        this.palavra = palavra;
        indices.add(indice);
    }
    @Override
    public String toString(){
        String aux = "palavra : " + this.palavra + "\n" + "indices : ";
        for(Integer aux2:this.indices){
            aux  = aux + aux2 + "\t";
        }
        return aux;
    }
}
