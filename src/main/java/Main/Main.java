/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Classes.ArquivoInvertido;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author kelvin
 */
public class Main {
    static int achaRepeticoes(List<ArquivoInvertido> lista,String palavra){
        Iterator<ArquivoInvertido> e = lista.iterator();
        ArquivoInvertido aux;
        while(e.hasNext()){
            aux = e.next();
            if(aux.palavra.equals(palavra)){
                return lista.indexOf(aux);
            }
        }
        return -1;
    }
    static void preencheArquivoInvertido(List<ArquivoInvertido> arquivoInvertido,String texto){
        String auxiliar = "";
        int indice;
        int inicioPalavra = 0;
        for(int i = 0;i < texto.length();i++){
            if(texto.charAt(i) == 32 ||texto.charAt(i) == 45 ||texto.charAt(i) == 46 || texto.charAt(i) == 44 || texto.charAt(i) == 58 || texto.charAt(i) == 34 || texto.charAt(i) == 63 || texto.charAt(i) == 33|| texto.charAt(i) == 10){
                if(!auxiliar.equals("")){
                    if(achaRepeticoes(arquivoInvertido,auxiliar) >= 0){
                        indice = achaRepeticoes(arquivoInvertido,auxiliar);
                        arquivoInvertido.get(indice).indices.add(inicioPalavra);
                    }
                    else{
                        arquivoInvertido.add(new ArquivoInvertido(auxiliar,inicioPalavra));
                    }
                }
                auxiliar = "";
                auxiliar += texto.charAt(i);
                inicioPalavra = i;
                if(achaRepeticoes(arquivoInvertido,auxiliar) >= 0){
                    indice = achaRepeticoes(arquivoInvertido,auxiliar);
                    arquivoInvertido.get(indice).indices.add(inicioPalavra);
                }
                else{
                    arquivoInvertido.add(new ArquivoInvertido(auxiliar,inicioPalavra));
                }
                auxiliar = "";
                inicioPalavra = i + 1;
            }
            else{
                auxiliar += texto.charAt(i);
            }
        }
    }
    static void mostraLista(List<ArquivoInvertido> lista){
        for(ArquivoInvertido aux2 : lista){
            System.out.println(aux2.toString());
        }
    }
    static String preencheTexto() throws Exception{
        String texto;
        String aux;
        int indice;
        int inicioPalavra = 0,j;
        BufferedReader arquivo = new BufferedReader(new InputStreamReader(new FileInputStream("C:/Users/Mks/Documents/NetBeansProjects/EstruturadeDados/src/main/java/Historia.txt"), "UTF-8"));
        texto = arquivo.readLine();
        while((aux = arquivo.readLine()) != null){
            texto  = texto + aux + "\n"; 
        }
        arquivo.close();
        return texto;
    }
    static void organizaLista(List<ArquivoInvertido> lista){
        int menorIndice;
        ArquivoInvertido aux;
        for(int i = 0;i < lista.size();i++){
            menorIndice = i;
            for(int k = i;k < lista.size();k++){
                if(lista.get(menorIndice).palavra.compareTo(lista.get(k).palavra) > 0){
                    menorIndice = k;
                }
            }
            aux = lista.get(i);
            lista.set(i,lista.get(menorIndice));
            lista.set(menorIndice,aux);
        }
    }
    static int buscaBinaria(List<ArquivoInvertido> lista, String busca,int inicio,int fim){
        int meio;
        int aux;
        if(inicio > fim){
            return -1;//não encontrou
        }
        meio = (inicio + fim)/2;
        aux = busca.compareTo(lista.get(meio).palavra);
        if(aux > 0){
            return buscaBinaria(lista,busca,meio + 1,fim);
        }
        else if(aux < 0){
            return buscaBinaria(lista,busca,inicio,meio - 1);
        }
        else{
            return meio;
        }
    }
            
    public static void main(String[] args)throws Exception {
        String texto = preencheTexto(),palavra,msg = "";
        Scanner scanner;
        int indice,aux,cont = 0,indiceTexto;
        Boolean op;
        List<ArquivoInvertido> arquivoInvertido = new ArrayList<>();
        preencheArquivoInvertido(arquivoInvertido,texto);
        organizaLista(arquivoInvertido);
        mostraLista(arquivoInvertido);
        System.out.println("digite a palavra para efetuar a busca:");
        scanner = new Scanner(System.in);
        palavra = scanner.next();
        indice = buscaBinaria(arquivoInvertido,palavra,0,arquivoInvertido.size());
        if(indice < 0){
            System.out.println("palavra não encontrada");
            return;
        }
        System.out.println("indice no arquivo invertido : " + indice);
        System.out.println(arquivoInvertido.get(indice));
        System.out.println("mostrar aparições ?");
        scanner = new Scanner(System.in);
        op = scanner.nextBoolean();
        while(op){
            if(cont < arquivoInvertido.get(indice).indices.size()){
                aux = 0;
                indiceTexto = arquivoInvertido.get(indice).indices.get(cont);
                for(int i = indiceTexto;i >=0 && aux <=3;i--){
                    msg = texto.charAt(i) + msg;
                    if(texto.charAt(i) == ' '){
                        aux++;
                    }
                }
                aux = 0;
                for(int i = indiceTexto + 1;i < texto.length() && aux <=3;i++){
                    msg += texto.charAt(i);
                    if(texto.charAt(i) == ' '){
                        aux++;
                    }
                }
                System.out.println("frase:" + msg);
                System.out.println("gostaria de mostrar mais aparições ?");
                scanner = new Scanner(System.in);
                op = scanner.nextBoolean();
                msg = "";
                cont++;
            }
            else{
                System.out.println("não há mais aparições");
                op = false;
            }
        }
    }
}
